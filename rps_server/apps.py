from django.apps import AppConfig


class RpsServerConfig(AppConfig):
    name = 'rps_server'
