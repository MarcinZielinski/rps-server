from django.urls import path

from . import views

urlpatterns = [
    path('submit/', views.submit_image, name='submit_image'),
    path('<int:player_id>/', views.get_result, name='get_result')
]
