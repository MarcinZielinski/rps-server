import cv2
import numpy as np
import copy
import math


# from appscript import app

# Environment:
# OS    : Mac OS EL Capitan
# python: 3.5
# opencv: 2.4.13


class HandPoseEstimation:
    def __init__(self):
        self.cap_region_x_begin = 0.5  # start point/total width
        self.cap_region_y_end = 0.8  # start point/total width
        self.threshold = 60  # BINARY threshold
        self.blurValue = 41  # GaussianBlur parameter
        self.bgSubThreshold = 50
        self.learningRate = 0

    def removeBG(self, fromFrame, bgModel):
        fgmask = bgModel.apply(fromFrame)
        # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        # res = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)

        kernel = np.ones((3, 3), np.uint8)
        fgmask = cv2.erode(fgmask, kernel, iterations=1)
        res = cv2.bitwise_and(fromFrame, fromFrame, mask=fgmask)
        return res

    def calculateFingers(self, res, drawing):  # -> finished bool, cnt: finger count
        #  convexity defect
        hull = cv2.convexHull(res, returnPoints=False)
        if len(hull) > 3:
            defects = cv2.convexityDefects(res, hull)
            if type(defects) != type(None):  # avoid crashing.   (BUG not found)

                cnt = 0
                for i in range(defects.shape[0]):  # calculate the angle
                    s, e, f, d = defects[i][0]
                    start = tuple(res[s][0])
                    end = tuple(res[e][0])
                    far = tuple(res[f][0])
                    a = math.sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
                    b = math.sqrt((far[0] - start[0]) ** 2 + (far[1] - start[1]) ** 2)
                    c = math.sqrt((end[0] - far[0]) ** 2 + (end[1] - far[1]) ** 2)
                    angle = math.acos((b ** 2 + c ** 2 - a ** 2) / (2 * b * c))  # cosine theorem
                    if angle <= math.pi / 2:  # angle less than 90 degree, treat as fingers
                        cnt += 1
                        cv2.circle(drawing, far, 8, [211, 84, 0], -1)
                return True, cnt
        return False, 0

    """
    it is one time use method, so to capture new background, create new HandPostEstimation object
    """
    def captureBackground(self, backgroundPath):
        frame = cv2.imread(backgroundPath)
        # camera.set(10, 200) brightness set to 20

        frame = cv2.bilateralFilter(frame, 5, 50, 100)  # smoothing filter
        # frame = cv2.flip(frame, 1)  # flip the frame horizontally
        # formula for rectangle width/height
        # cv2.rectangle(frame, (int(cap_region_x_begin * frame.shape[1]), 0),
        #               (frame.shape[1], int(cap_region_y_end * frame.shape[0])), (255, 0, 0), 2)
        # cv2.imshow('original', frame)

        bgModel = cv2.createBackgroundSubtractorMOG2(0, self.bgSubThreshold)
        bgModel.apply(frame)
        return bgModel

    def captureImageAndGetResult(self, imagePath, bgModel):
        frame = cv2.imread(imagePath)
        img = self.removeBG(frame, bgModel)
        img = img[0:int(self.cap_region_y_end * frame.shape[0]),
              int(self.cap_region_x_begin * frame.shape[1]):frame.shape[1]]  # clip the ROI
        # cv2.imshow('mask', img)

        # convert the image into binary image
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (self.blurValue, self.blurValue), 0)
        # cv2.imshow('blur', blur)
        ret, thresh1 = cv2.threshold(blur, self.threshold, 255, cv2.THRESH_BINARY)
        # cv2.imshow('ori', thresh)

        # get the coutours
        # thresh1 = copy.deepcopy(thresh)
        contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        length = len(contours)
        maxArea = -1
        if length > 0:
            for i in range(length):  # find the biggest contour (according to area)
                temp = contours[i]
                area = cv2.contourArea(temp)
                if area > maxArea:
                    maxArea = area
                    ci = i

            res = contours[ci]
            # hull = cv2.convexHull(res)
            drawing = np.zeros(img.shape, np.uint8)
            cv2.drawContours(drawing, [res], 0, (0, 255, 0), 2)
            # cv2.drawContours(drawing, [hull], 0, (0, 0, 255), 3)

            isFinishCal, cnt = self.calculateFingers(res, drawing)
            # cv2.imshow('output', drawing) # UNCOMMENT TO SEE RESULT HAND
            # cv2.waitKey(10) # UNCOMMENT THIS ALSO
            if isFinishCal:
                print(cnt)
                if cnt == 0:
                    result = "rock"
                elif cnt >= 3:
                    result = "paper"
                else:
                    result = "scissors"
                return result
            return "error"
        return "error"

        # Keyboard OP
        # k = cv2.waitKey(10)

############################## OLD STUFFFFFFFFFFFFFFFFFFFFFF

# from __future__ import division
# import cv2
# import time
# import numpy as np
#
# POSE_PAIRS = [[0, 1], [1, 2], [2, 3], [3, 4], [0, 5], [5, 6], [6, 7], [7, 8], [0, 9], [9, 10], [10, 11], [11, 12],
#               [0, 13], [13, 14], [14, 15], [15, 16], [0, 17], [17, 18], [18, 19], [19, 20], [20, 21]]
#
#
# def preparePhoto(imagePath):
#     protoFile = "../hand/pose_deploy.prototxt"
#     weightsFile = "../hand/pose_iter_102000.caffemodel"
#     nPoints = 22
#     net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)
#
#     frame = cv2.imread(imagePath)
#     frameCopy = np.copy(frame)
#     frameWidth = frame.shape[1]
#     frameHeight = frame.shape[0]
#     aspect_ratio = frameWidth / frameHeight
#
#     threshold = 0.1
#
#     t = time.time()
#     # input image dimensions for the network
#     inHeight = 368
#     inWidth = int(((aspect_ratio * inHeight) * 8) // 8)
#     inpBlob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (inWidth, inHeight), (0, 0, 0), swapRB=False, crop=False)
#
#     net.setInput(inpBlob)
#
#     output = net.forward()
#     print("time taken by network : {:.3f}".format(time.time() - t))
#     # Empty list to store the detected keypoints
#     points = []
#
#     for i in range(nPoints):
#         # confidence map of corresponding body's part.
#         probMap = output[0, i, :, :]
#         probMap = cv2.resize(probMap, (frameWidth, frameHeight))
#
#         # Find global maxima of the probMap.
#         minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)
#
#         if prob > threshold:
#             cv2.circle(frameCopy, (int(point[0]), int(point[1])), 8, (0, 255, 255), thickness=-1, lineType=cv2.FILLED)
#             cv2.putText(frameCopy, "{}".format(i), (int(point[0]), int(point[1])), cv2.FONT_HERSHEY_SIMPLEX, 1,
#                         (0, 0, 255),
#                         2, lineType=cv2.LINE_AA)
#
#             # Add the point to the list if the probability is greater than the threshold
#             points.append((int(point[0]), int(point[1])))
#         else:
#             points.append(None)
#
#     fingerNum = -1
#     fingers = {}
#     zeroPoint = points[0]
#     POINTS_NUMS = range(0, 21)
#
#     for x, y in zip(points, POINTS_NUMS):
#         if y == 0:
#             continue
#
#         if y % 4 == 1:
#             fingerNum += 1
#             fingers[fingerNum] = []
#         print(str(x) + " " + str(y))
#         fingers[fingerNum].append(x)
#
#     print("ZERO POINT: {} {}".format(zeroPoint[0], zeroPoint[1]))
#
#     def countDistance(x1, y1, x2, y2):
#         return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5
#
#     distancesFromTipToZero = []
#
#     for fingerNum, p in fingers.items():
#         if fingerNum == 0:
#             continue
#
#         x1, y1 = zeroPoint
#         x2, y2 = p[len(p) - 1]
#
#         distance = countDistance(x1, y1, x2, y2)
#         distancesFromTipToZero.append(distance)
#
#     print(distancesFromTipToZero)
#
#     distancesFromZeroToEveryPointsExceptTip = {}
#
#     for fingerNum, p in fingers.items():
#         if fingerNum == 0:
#             continue
#
#         x1, y1 = zeroPoint
#
#         distancesFromZeroToEveryPointsExceptTip[fingerNum] = []
#
#         for point in p:
#             if point == p[len(p) - 1]:
#                 break
#             x2, y2 = point
#             distance = countDistance(x1, y1, x2, y2)
#             distancesFromZeroToEveryPointsExceptTip[fingerNum].append(distance)
#
#     draw_skeleton(points, frame, frameCopy)
#
#     # check first 2 fingers (not including thumb) for being a scissors xd
#     firstClosed = isDistanceCloserFromPointsTo(distancesFromZeroToEveryPointsExceptTip[1], distancesFromTipToZero[0])
#     secondClosed = isDistanceCloserFromPointsTo(distancesFromZeroToEveryPointsExceptTip[2], distancesFromTipToZero[1])
#
#     if firstClosed and secondClosed:
#         result = "rock"
#
#     thirdClosed = isDistanceCloserFromPointsTo(distancesFromZeroToEveryPointsExceptTip[3], distancesFromTipToZero[2])
#     fourthClosed = isDistanceCloserFromPointsTo(distancesFromZeroToEveryPointsExceptTip[4], distancesFromTipToZero[3])
#
#     if thirdClosed and fourthClosed:
#         result = "scissors"
#     result = "paper"
#
#     print(result)
#     return result
#
#
# def isDistanceCloserFromPointsTo(distancesToAnyPoint: any, distanceFromTipToZero: float):
#     for distToPoint in distancesToAnyPoint:
#         if distToPoint >= distanceFromTipToZero:
#             return True
#     return False
#
#
# def draw_skeleton(points, frame, frameCopy):
#     # Draw Skeleton
#     for pair in POSE_PAIRS:
#         partA = pair[0]
#         partB = pair[1]
#
#         if points[partA] and points[partB]:
#             cv2.line(frame, points[partA], points[partB], (0, 255, 255), 2)
#             cv2.circle(frame, points[partA], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)
#             cv2.circle(frame, points[partB], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)

# uncomment when debugging
# cv2.imshow('Output-Keypoints', frameCopy)
# cv2.imshow('Output-Skeleton', frame)

# cv2.imwrite('Output-Keypoints.jpg', frameCopy)
# cv2.imwrite('Output-Skeleton.jpg', frame)

# cv2.waitKey(0)
