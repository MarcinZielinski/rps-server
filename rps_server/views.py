import base64
import uuid
from time import sleep

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from rps_server.game_controller import GameController
from rps_server.handPoseImage import HandPoseEstimation

game_controller = GameController()


@csrf_exempt
@api_view(['POST'])
def submit_image(request):
    if request.method == 'POST':
        choice = get_rps(request.data['bg'], request.data['img'])
        player_id = game_controller.submit_choice(choice)
        resp = {
            'player_id': player_id,
            'recognized_rps': choice
        }
        return JsonResponse(resp)
    pass


@csrf_exempt
@api_view(['GET'])
def get_result(request, player_id):
    while not game_controller.is_results_available():
        sleep(0.1)
    return JsonResponse({'result': game_controller.get_result(player_id)})


def get_rps(background, to_estimate):
    bg_path = save_image(base64.b64decode(bytes(background, 'utf-8')))
    to_estimate_path = save_image(base64.b64decode(bytes(to_estimate, 'utf-8')))

    hand_pose_estimation = HandPoseEstimation()
    bg_model = hand_pose_estimation.captureBackground("images/" + bg_path)
    type_of_image = hand_pose_estimation.captureImageAndGetResult("images/" + to_estimate_path, bg_model)

    return type_of_image


def save_image(to_save):
    image_name = str(uuid.uuid4()) + ".png"
    with open("images/" + image_name, 'wb') as f:
        f.write(to_save)
    return image_name
