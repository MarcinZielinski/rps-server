
game_map = {
    ('paper', 'paper'): 2,
    ('rock', 'rock'): 2,
    ('scissors', 'scissors'): 2,
    ('paper', 'scissors'): 1,
    ('rock', 'paper'): 1,
    ('scissors', 'rock'): 1,
    ('rock', 'scissors'): 0,
    ('paper', 'rock'): 0,
    ('scissors', 'paper'): 0
}


class GameController:
    player_choices = {}

    game_end = False

    results_returned = 0

    def submit_choice(self, choice):
        player_id = len(self.player_choices)
        self.player_choices[player_id] = choice
        if len(self.player_choices) == 2:
            self.game_end = True
        return player_id

    def get_result(self, player_id):
        winner = game_map[(self.player_choices[0], self.player_choices[1])]
        self.results_returned += 1
        if self.results_returned == 2:
            self.clear_game()
        if winner == player_id:
            return 'You win'
        elif winner == 2:
            return 'Draw'
        else:
            return 'You lose'

    def is_results_available(self):
        return len(self.player_choices) == 2

    def clear_game(self):
        self.player_choices.clear()
        self.results_returned = 0
        self.game_end = False
