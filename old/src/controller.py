from http.server import BaseHTTPRequestHandler
import base64
from src.handPoseImage import HandPoseEstimation
import uuid


class Controller(BaseHTTPRequestHandler):

    def do_OPTIONS(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Headers', 'bg-path')
        self.end_headers()
        self.wfile.write(bytes('ok', "utf8"))

    def do_POST(self):
        content_len = int(self.headers.get('Content-Length'))
        base64Image = self.rfile.read(content_len)

        costam = base64.b64decode(bytes(base64Image))

        imageName = str(uuid.uuid4()) + ".png"
        with open("../images/" + imageName, 'wb') as f:
            f.write(costam)

        bgImageName = self.headers.get('bg-path')

        if bgImageName is None:
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(bytes('{"backgroundPath": "' + imageName + '"}', "utf8"))
        else:
            bgImageName = str(bgImageName)
            handPoseEstimation = HandPoseEstimation()
            bgModel = handPoseEstimation.captureBackground("../images/" + bgImageName)
            typeOfImage = handPoseEstimation.captureImageAndGetResult("../images/" + imageName, bgModel)

            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()

            self.wfile.write(bytes('{"image": "' + typeOfImage + '"}', "utf8"))
